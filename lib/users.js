/*!
 * s0cket-sdk
 * users.js
 * Copyright(c) 2014 Vikas Jayaram <vikas@bizarmobile.com.au>, David Baxter <david@socketcentral.com>
 * MIT Licensed
 */
var HttpClient = require('node-rest-client').Client;
var config = require('./config');
httpClient = new HttpClient();
module.exports = function(server, appId, userKey, token) {
    var endPoint = server;

    function usersApi(options) {
        options = options || {};
        this._sendPost = function(action, jsonData, cb) {
            var msg = jsonData ? jsonData : {};
            msg.appId = appId;
            var args = {
                path: {
                    action: config.apiBasePath + action
                },
                data: msg,
                headers: {
                    "Content-Type": "application/json",
                    "X-Socket-API-Key": appId,
                    "X-Socket-API-User": userKey,
                    "X-Socket-API-Token": token
                }
            };
            console.log(args);
            httpClient.post(endPoint + "/${action}", args, function(data, response) {
                if (response.statusCode === 200) {
                    cb(null, data);
                } else {
                    cb(data);
                }
            });
        };
        this._sendPut = function(action, jsonData, id, cb) {
            var msg = jsonData ? jsonData : {};
            msg.appId = appId;
            var args = {
                path: {
                    action: config.apiBasePath + action,
                    id: id
                },
                data: msg,
                headers: {
                    "Content-Type": "application/json",
                    "X-Socket-API-Key": appId,
                    "X-Socket-API-User": userKey,
                    "X-Socket-API-Token": token
                }
            };
            console.log(args);
            httpClient.put(endPoint + "/${action}/${id}", args, function(data, response) {
                if (response.statusCode === 200) {
                    cb(data);
                } else {
                    cb(data);
                }
            });
        };
        this._sendGet = function(action, id, cb) {
            var msg = {};
            msg.appId = appId;
            var args = {
                path: {
                    action: config.apiBasePath + action,
                    id: id
                },
                data: msg,
                headers: {
                    "Content-Type": "application/json",
                    "X-Socket-API-Key": appId,
                    "X-Socket-API-User": userKey,
                    "X-Socket-API-Token": token
                }
            };
            console.log(args);
            httpClient.get(endPoint + "/${action}/${id}", args, function(data, response) {
                if (response.statusCode === 200) {
                    cb(data);
                } else {
                    cb(data);
                }
            });
        };
        this._sendDelete = function(action, id, cb) {
            var msg = {};
            msg.appId = appId;
            var args = {
                path: {
                    action: config.apiBasePath + action,
                    id: id
                },
                data: msg,
                headers: {
                    "Content-Type": "application/json",
                    "X-Socket-API-Key": appId,
                    "X-Socket-API-User": userKey,
                    "X-Socket-API-Token": token
                }
            };
            console.log(args);
            httpClient.delete(endPoint + "/${action}/${id}", args, function(data, response) {
                if (response.statusCode === 200) {
                    cb(data);
                } else {
                    cb(data);
                }
            });
        };
    }
    usersApi.prototype.getusers = function(id, cb) {
        this._sendGet('users', id, cb);
    };
    usersApi.prototype.createUser = function(jsonData, cb) {
        var data = jsonData ? jsonData : {};
        this._sendPost('auth/connect/register', data, null, cb);
    };
/*  usersApi.prototype.del = function (id, cb) {
    this._sendDelete('post', id, cb);
  };*/
    return new usersApi();
};