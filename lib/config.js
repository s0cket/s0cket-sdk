module.exports = {
	apiBasePath: 'api/1/',
	apikey: 'X-Socket-API-Key',
	userkey: 'X-Socket-API-User',
	tokenkey: 'X-Socket-API-Token',
}