/*!
 * s0cket-sdk
 * authenticate.js
 * Copyright(c) 2014 Vikas Jayaram <vikas@bizarmobile.com.au>, David Baxter <david@socketcentral.com>
 * MIT Licensed
 */
var HttpClient = require('node-rest-client').Client;

var config = require('./config');
httpClient = new HttpClient();


module.exports = function(server) {
  var endPoint  = server;

  function authenticate(options) {
    options = options || {};
    var apiKey = config.apiKey;
    this._login = function(action, appId, jsonData, cb) {
      var msg = jsonData ? jsonData : {};
      msg.appId = appId;
      var args = {
        path     : { action : config.apiBasePath + action },
        data     : msg,
        headers  : {
                      "Content-Type": "application/json",
                      "X-Socket-API-Key": appId
                   }
      };
      console.log(args);
      httpClient.post(endPoint + "/${action}", args, function (data, response) {
        if (response.statusCode === 200) {
          console.log(data);
          cb(data);
        } else {
          cb(data);
        }
      });
    };
  }

  authenticate.prototype.login = function (appId, jsonData, cb) {
    var data = jsonData ? jsonData : {};
    this._login('/auth/login', appId, data, cb);
  };

  return new authenticate();

};
