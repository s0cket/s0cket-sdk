/*!
 * s0cket-sdk
 * pushnotification.js
 * Copyright(c) 2014 Vikas Jayaram <vikas@bizarmobile.com.au>, David Baxter <david@socketcentral.com>
 * MIT Licensed
 */
var HttpClient = require('node-rest-client').Client;

httpClient = new HttpClient();


module.exports = function(server) {
  var endPoint  = server;

  function pushServer(options) {
    options = options || {};

    this._sendPost = function(action, appId, jsonData, cb) {
      var msg = jsonData ? jsonData : {};
      msg.appId = appId;
      var args = {
        path     : { action : action,
                     apiKey : appId },
        data     : msg,
        headers  : {"Content-Type": "application/json"}
      };
      console.log(args);
      httpClient.post(endPoint + "/${action}/${apiKey}", args, function (data, response) {
        if (response.statusCode === 200) {
          cb("success");
        } else {
          cb("error");
        }
      });
    };
  }

  pushServer.prototype.subscribe = function (appId, jsonData, cb) {
    var data = jsonData ? jsonData : {};
    this._sendPost('subscribe', appId, data, cb);
  };

  pushServer.prototype.unsubscribe = function (appId, jsonData, cb) {
    var data = jsonData ? jsonData : {};
    this._sendPost('unsubscribe', appId, data, cb);
  };

  pushServer.prototype.send = function (appId, jsonData, cb) {
    var data = jsonData ? jsonData : {};
    this._sendPost('send', appId, data, cb);
  };

  return new pushServer();

};
