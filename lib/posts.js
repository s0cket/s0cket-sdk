/*!
 * s0cket-sdk
 * posts.js
 * Copyright(c) 2014 Vikas Jayaram <vikas@bizarmobile.com.au>, David Baxter <david@socketcentral.com>
 * MIT Licensed
 */
var HttpClient = require('node-rest-client').Client;
var config = require('./config');

httpClient = new HttpClient();


module.exports = function(server, appId, userKey, token) {
  var endPoint  = server;

  function postsApi(options) {
    options = options || {};

    this._sendPost = function(action, type, jsonData, cb) {
      var msg = jsonData ? jsonData : {};
      msg.appId = appId;
      var args = {
        path     : { action : config.apiBasePath + action,
                     type : type },
        data     : msg,
        headers  : {"Content-Type": "application/json",
                      "X-Socket-API-Key": appId,
                      "X-Socket-API-User": userKey,
                      "X-Socket-API-Token": token

                    }
      };
      console.log(args);
      httpClient.post(endPoint + "/${action}/${type}", args, function (data, response) {
        if (response.statusCode === 200) {
          cb(data);
        } else {
          cb(data);
        }
      });
    };
    this._sendGet = function(action, type, cb) {
      var msg = {};
      msg.appId = appId;
      var args = {
        path     : { action : config.apiBasePath + action,
                     type : type },
        data     : msg,
        headers  : {"Content-Type": "application/json",
                      "X-Socket-API-Key": appId,
                      "X-Socket-API-User": userKey,
                      "X-Socket-API-Token": token

                    }
      };
      console.log(args);
      httpClient.get(endPoint + "/${action}/${type}", args, function (data, response) {
        if (response.statusCode === 200) {
          cb(data);
        } else {
          cb(data);
        }
      });
    };
    this._sendDelete = function(action, id, cb) {
      var msg = {};
      msg.appId = appId;
      var args = {
        path     : { action : config.apiBasePath + action,
                     id : id },
        data     : msg,
        headers  : {"Content-Type": "application/json",
                      "X-Socket-API-Key": appId,
                      "X-Socket-API-User": userKey,
                      "X-Socket-API-Token": token
                    }
      };
      console.log(args);
      httpClient.delete(endPoint + "/${action}/${id}", args, function (data, response) {
        if (response.statusCode === 200) {
          cb(data);
        } else {
          cb(data);
        }
      });
    };

}

  postsApi.prototype.getPosts = function (type, cb) {
    this._sendGet('posts', type, cb);
  };

  postsApi.prototype.createPost = function (type, jsonData, cb) {
    var data = jsonData ? jsonData : {};
    this._sendPost('posts', type, data, cb);
  };

  postsApi.prototype.del = function (id, cb) {
    this._sendDelete('post', id, cb);
  };

  return new postsApi();

};
